#include "DigiLed.h"
#include "spi.h"
#include "stdint.h"

void sendRaw(uint8_t data) {
	HAL_SPI_Transmit(&hspi1, &data, 1, HAL_MAX_DELAY);
}

void update() {
	// StartFrame (0x00000000)
	sendRaw(0x00);
	sendRaw(0x00);
	sendRaw(0x00);
	sendRaw(0x00);

	for (int led = 0; led < NUMBER_OF_LEDS; led++) {
		int ledNum = led*3;
		// the 3 INIT bits should always be 0xE0 (11100000) + the brightness bits should also all be ones 0x1F (00011111) = 0xFF
		sendRaw((uint8_t) (0xFF) );
		sendRaw((uint8_t) (ledBuffer[ledNum]));   // Send BLUE
		sendRaw((uint8_t) (ledBuffer[ledNum+1])); // Send GREEN
		sendRaw((uint8_t) (ledBuffer[ledNum+2])); // Send RED
	}

	// EndFrame (0xffffffff)
	sendRaw(0xff);
	sendRaw(0xff);
	sendRaw(0xff);
	sendRaw(0xff);

}

uint8_t TestPosition(const uint8_t led) {
	uint8_t returnValue = OUT_OF_RANGE;
	if (led < bufferLength) {
		returnValue = RANGE_OK;
	}
	return returnValue;
}


void setColor( uint8_t led, uint8_t red,  uint8_t green, uint8_t blue ) {
	int ledNum = led*3;
	if (TestPosition(ledNum) == RANGE_OK) {
		ledBuffer[ledNum] = blue;
		ledBuffer[ledNum+1] = green;
		ledBuffer[ledNum+2] = red;
	}
}

void setRGB( uint8_t led, uint32_t rgb)
{
	setColor(led, (uint8_t)(rgb), (uint8_t)(rgb >> 8), (uint8_t)(rgb >> 16 ));
}

uint32_t Color(uint8_t r, uint8_t g, uint8_t b) {
  return ((uint32_t)b << 16) | ((uint32_t)g <<  8) | r;
}


