/*
 * colors.h
 *
 *  Created on: 5 paź 2021
 *      Author: narut
 */

#ifndef INC_COLORS_H_
#define INC_COLORS_H_

#define BLACK  0x000000
#define WHITE  0x646464
#define BLUE   0x640000
#define RED    0x000064
#define GREEN  0x006400
#define PURPLE 0x490049
#define PINK   0x381e6b
#define ORANGE 0x01699c
#define YELLOW 0x23a6af
#define CYAN   0x80af23

#endif /* INC_COLORS_H_ */
