/*
 * DigiLed.h
 *
 *  Created on: Oct 10, 2021
 *      Author: narut
 */

#ifndef INC_DIGILED_H_
#define INC_DIGILED_H_

#define NUMBER_OF_LEDS 13	// Define number of LEDs in the chain
#define COLORS_PER_LED 3	// Define number of colors we store
#define OUT_OF_RANGE 1
#define RANGE_OK 0

const int bufferLength = NUMBER_OF_LEDS*3;
uint8_t ledBuffer[NUMBER_OF_LEDS*COLORS_PER_LED];

void sendRaw(uint8_t data);
void update();
uint8_t TestPosition(const uint8_t led);
void setColor( uint8_t led, uint8_t red,  uint8_t green, uint8_t blue );
void setRGB( uint8_t led, uint32_t rgb);
uint32_t Color(uint8_t r, uint8_t g, uint8_t b);


#endif /* INC_DIGILED_H_ */
